const Router = require("koa-router");
const router = new Router();
const Middleware = require("../middlewares");
const Controller = require("./auth/controller");

router
  .post("/login", Controller.login)
  .get("/admin", Middleware.authenticate, Middleware.isAdmin, Controller.getAdmin)
  .get("/scraper", Middleware.authenticate, Middleware.isScraper, Controller.getScraper)
  .get("/user", Middleware.authenticate, Controller.getUser);

module.exports = router;
